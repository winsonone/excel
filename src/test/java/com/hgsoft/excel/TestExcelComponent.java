/**
 * 
 */
package com.hgsoft.excel;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author wenweicong
 * 
 */
public class TestExcelComponent {

	@Test
	public void testGenerateSingleSheetExcelFile() {
		File template = new File("src/main/resources/template.xls");
		File target = new File("src/main/resources/out.xls");
		// 准备离散数据
		Map<String, Object> discreteData = new HashMap<String, Object>();
		discreteData.put("sumOfDog", 100);
		discreteData.put("sumOfCat", 130);
		// 准备列表数据
		List<Map<String, Object>> listData = new ArrayList<>();
		Map<String, Object> map1 = new HashMap<>();
		map1.put("province", "广东省");
		map1.put("county", "广州市");
		map1.put("numOfDog", 30);
		map1.put("numOfCat", 40);
		listData.add(map1);
		Map<String, Object> map2 = new HashMap<>();
		map2.put("province", "广东省");
		map2.put("county", "肇庆市");
		map2.put("numOfDog", 50);
		map2.put("numOfCat", 60);
		listData.add(map2);
		Map<String, Object> map3 = new HashMap<>();
		map3.put("province", "江苏省");
		map3.put("county", "东台市");
		map3.put("numOfDog", 20);
		map3.put("numOfCat", 30);
		listData.add(map3);
		String[] columns = new String[] { "province", "county", "numOfDog",
				"numOfCat" };
		try {
			boolean isSuccess = ExcelComponent.generateSingleSheetExcelFile(
					template, target, discreteData, listData, columns, "list0");
			Assert.assertEquals(true, isSuccess);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGenerateSingleSheetExcelFileSupportMergeSameCell() {
		File template = new File("src/main/resources/template.xls");
		File target = new File("src/main/resources/out_merge.xls");
		// 准备离散数据
		Map<String, Object> discreteData = new HashMap<String, Object>();
		discreteData.put("sumOfDog", 100);
		discreteData.put("sumOfCat", 130);
		// 准备列表数据
		List<Map<String, Object>> listData = new ArrayList<>();
		Map<String, Object> map1 = new HashMap<>();
		map1.put("province", "广东省");
		map1.put("county", "广州市");
		map1.put("numOfDog", 30);
		map1.put("numOfCat", 40);
		listData.add(map1);
		Map<String, Object> map2 = new HashMap<>();
		map2.put("province", "广东省");
		map2.put("county", "肇庆市");
		map2.put("numOfDog", 50);
		map2.put("numOfCat", 60);
		listData.add(map2);
		Map<String, Object> map3 = new HashMap<>();
		map3.put("province", "江苏省");
		map3.put("county", "东台市");
		map3.put("numOfDog", 20);
		map3.put("numOfCat", 30);
		listData.add(map3);
		String[] columns = new String[] { "province", "county", "numOfDog",
				"numOfCat" };
		int[] mergeColumns = new int[] { 0 };
		try {
			boolean isSuccess = ExcelComponent
					.generateSingleSheetExcelFileSupportMergeSameCell(template,
							target, discreteData, listData, columns, "list0",
							mergeColumns);
			Assert.assertEquals(true, isSuccess);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGenerateMultiSheetExcelFile() {
		File template = new File("src/main/resources/template_multi.xls");
		File target = new File("src/main/resources/out_multi.xls");
		// 准备离散数据
		Map<String, Object> discreteData = new HashMap<String, Object>();
		discreteData.put("sumOfDog", 100);
		discreteData.put("sumOfCat", 130);
		discreteData.put("sumOfPeople", 100);
		// 准备列表数据
		Map<String, List<Map<String, Object>>> listDataMap = new HashMap<>();
		List<Map<String, Object>> listData = new ArrayList<>();
		Map<String, Object> map1 = new HashMap<>();
		map1.put("province", "广东省");
		map1.put("county", "广州市");
		map1.put("numOfDog", 30);
		map1.put("numOfCat", 40);
		listData.add(map1);
		Map<String, Object> map2 = new HashMap<>();
		map2.put("province", "广东省");
		map2.put("county", "肇庆市");
		map2.put("numOfDog", 50);
		map2.put("numOfCat", 60);
		listData.add(map2);
		Map<String, Object> map3 = new HashMap<>();
		map3.put("province", "江苏省");
		map3.put("county", "东台市");
		map3.put("numOfDog", 20);
		map3.put("numOfCat", 30);
		listData.add(map3);
		listDataMap.put("list0", listData);
		List<Map<String, Object>> listData1 = new ArrayList<>();
		Map<String, Object> map4 = new HashMap<>();
		map4.put("province", "广东省");
		map4.put("county", "广州市");
		map4.put("numOfPeople", 30);
		listData1.add(map4);
		Map<String, Object> map5 = new HashMap<>();
		map5.put("province", "广东省");
		map5.put("county", "肇庆市");
		map5.put("numOfPeople", 50);
		listData1.add(map5);
		Map<String, Object> map6 = new HashMap<>();
		map6.put("province", "江苏省");
		map6.put("county", "东台市");
		map6.put("numOfPeople", 20);
		listData1.add(map6);
		listDataMap.put("list1", listData1);
		String[] columns = new String[] { "province,county,numOfDog,numOfCat","province,county,numOfPeople" };
		String[] listNames = new String[]{"list0","list1"};
		try {
			boolean isSuccess = ExcelComponent.generateMultiSheetExcelFile(template, target, discreteData, listDataMap, columns, listNames);
			Assert.assertEquals(true, isSuccess);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGenerateMultiSheetExcelFileSupportMergeSameCell() {
		File template = new File("src/main/resources/template_multi.xls");
		File target = new File("src/main/resources/out_multi_merge.xls");
		// 准备离散数据
		Map<String, Object> discreteData = new HashMap<String, Object>();
		discreteData.put("sumOfDog", 100);
		discreteData.put("sumOfCat", 130);
		discreteData.put("sumOfPeople", 100);
		// 准备列表数据
		Map<String, List<Map<String, Object>>> listDataMap = new HashMap<>();
		List<Map<String, Object>> listData = new ArrayList<>();
		Map<String, Object> map1 = new HashMap<>();
		map1.put("province", "广东省");
		map1.put("county", "广州市");
		map1.put("numOfDog", 30);
		map1.put("numOfCat", 40);
		listData.add(map1);
		Map<String, Object> map2 = new HashMap<>();
		map2.put("province", "广东省");
		map2.put("county", "肇庆市");
		map2.put("numOfDog", 50);
		map2.put("numOfCat", 60);
		listData.add(map2);
		Map<String, Object> map3 = new HashMap<>();
		map3.put("province", "江苏省");
		map3.put("county", "东台市");
		map3.put("numOfDog", 20);
		map3.put("numOfCat", 30);
		listData.add(map3);
		listDataMap.put("list0", listData);
		List<Map<String, Object>> listData1 = new ArrayList<>();
		Map<String, Object> map4 = new HashMap<>();
		map4.put("province", "广东省");
		map4.put("county", "广州市");
		map4.put("numOfPeople", 30);
		listData1.add(map4);
		Map<String, Object> map5 = new HashMap<>();
		map5.put("province", "广东省");
		map5.put("county", "肇庆市");
		map5.put("numOfPeople", 50);
		listData1.add(map5);
		Map<String, Object> map6 = new HashMap<>();
		map6.put("province", "江苏省");
		map6.put("county", "东台市");
		map6.put("numOfPeople", 20);
		listData1.add(map6);
		listDataMap.put("list1", listData1);
		String[] columns = new String[] { "province,county,numOfDog,numOfCat","province,county,numOfPeople" };
		String[] listNames = new String[]{"list0","list1"};
		List<int[]> mergeColumns = new ArrayList<>();
		mergeColumns.add(new int[]{0});
		mergeColumns.add(new int[]{0});
		try {
			boolean isSuccess = ExcelComponent.generateMultiSheetExcelFileSupportMergeSameCell(template, target, discreteData, listDataMap, columns, listNames, mergeColumns);
			Assert.assertEquals(true, isSuccess);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
